<?php

class Product{   
    protected $sku;
    protected $name;
    protected $price;
    protected $description;
}
class Dvd extends Product{

    public function __construct()
    {
        $this->sku = $_POST['sku'];
        $this->name = $_POST['name'];
        $this->price = $_POST['price'];
        $this->description ='Size: '.$_POST['size'].' MB';
    }
    public function __get($name){
        return $this->$name;
    }

}
class Book extends Product{

    public function __construct()
    {
        $this->sku = $_POST['sku'];
        $this->name = $_POST['name'];
        $this->price = $_POST['price'];
        $this->description ='Weight: '.$_POST['weight'].' KG';
    }
    
    public function __get($name){
        return $this->$name;
    }
}
class Furniture extends Product{

    public function __construct()
    {
        $this->sku = $_POST['sku'];
        $this->name = $_POST['name'];
        $this->price = $_POST['price'];
        $this->description ='Dimensions: '.$_POST['height'].'x'.$_POST['width'].'x'.$_POST['length'];
    }
    public function __get($name){
        return $this->$name;
    }
}

?>