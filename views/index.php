<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" content="Product list">
        <title>Product Index</title>
        <link rel="stylesheet" href="style/style.css">
        <!--Scripts-->
        <script src="../controller/script.js"></script>
    </head>
    <body onload="uncheck()">
        <section class="top">
            <h1>Product List</h1>
            <div class="buttons-top">
                <a href="add.html"><button>Add</button></a>
                <input id='delete-btn' type="submit" form="indexForm" value="Mass Delete">
            </div>
            <div class="line" id="top-line"></div>
        </section>
        <form id="indexForm" method="POST" action="../controller/delete.php">
        <section class="index-middle">
            <?php
                $conn = new mysqli('localhost','id16320399_root','>9yX5j9WNGm(Kml=','id16320399_scandiweb','3306');
                
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                $sql = "SELECT * FROM products ORDER BY ID DESC";
                $get_data = $conn->query($sql);
                $result = $get_data->fetch_all();
                
                foreach($result as $obj){;
                    echo '<div class="prod-content">';
                    echo '<input type="checkbox" name="checkbox_list[]" value='.htmlspecialchars($obj[0]).'>';
                    echo '<p class="prod-desc">'.$obj[1].'</p>';
                    echo '<p class="prod-desc">'.$obj[2].'</p>';
                    echo '<p class="prod-desc">'.$obj[3].' $</p>';
                    echo '<p class="prod-desc">'.$obj[4].'</p>';
                    echo '</div>';
                }
                ?>
        </section>
        </form>
        <section class="bottom">
            <div class="line" id="bottom-line"></div>
            <p>Scandiweb Test assignment</p>
        </section>
    </body>

</html>
