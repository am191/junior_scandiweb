//script for index.php page
//clean all checkboxes on page reload/refresh
function uncheck(){
    var check = document.getElementsByName('checkbox_list[]');
    for(var x = 0; x<check.length; x++){
        if (check[x].checked = true) check[x].checked=false;
    }
}
//script for add.html page
//clean all input on page reload/refresh
function cleanInput(){
    var input = document.getElementsByClassName('input');
    for(var x = 0; x<input.length; x++){
        input[x].value = '';
    }
    document.getElementById("choice").value = '0';
}
//show hidden forms
function showForm(){
    var disc = document.getElementById("disc-div");
    var furniture = document.getElementById("furniture-div");
    var book = document.getElementById("book-div");
    var select = document.getElementById("choice").value;
    
    switch(select){
        case "0":
            disc.style.display="none";
            furniture.style.display="none";
            book.style.display="none";
            break;
        case "Dvd":
            disc.style.display="block";
            furniture.style.display="none";
            book.style.display="none";
            break;
        case "Furniture":
            disc.style.display="none";
            furniture.style.display="block";
            book.style.display="none";
            break;
        case "Book":
            disc.style.display="none";
            furniture.style.display="none";
            book.style.display="block";
            break;
    }
}

//calls the input check functions one more time before submitting 
function formValidation(){
    var option = document.getElementById('choice').value,
    retValue = true;    //variable is used instead of return statements to avoid bugs with unreachable code or not completing all the checks
    if (option == '0'){
        document.getElementById('select1').innerHTML="Please, select product type";
        retValue = false;
    }else{
        document.getElementById('select1').innerHTML="";
    }
    
    if (!SKUcheck()){
        retValue = false;
    }
    if (!nameCheck()) {
        retValue = false;
    }
    if(!priceCheck()){
        retValue = false;
    }

    switch(option){
        case '1':
            if(!sizeCheck()){
                retValue = false;
            }
            break;
        case'2':
            if(!widthCheck()){
                retValue = false;
            }
            if(!lengthCheck()){
                retValue = false;
            }
            if(!heightCheck()){
                retValue = false;
            }
            break;
        case'3':
            if(!weightCheck()){
                retValue = false;
            }
            break;
    }
    console.log(retValue);
    return retValue;
}

//individual input checks that run on individual input
function SKUcheck(){
    var sku = document.getElementById('sku').value,
    parText = document.getElementById('sku1'),
    check = /^[A-Z0-9]+$/;
    if (!sku){
        parText.innerHTML = "Please, submit required data";
        return false;
    }else if(!check.test(sku)){
        parText.innerHTML = "Please, provide the data of indicated type";
        return false;
    }else{
        parText.innerHTML = "";
        return true;
    }
}
function nameCheck(){
    var name = document.getElementById('name').value,
    parText = document.getElementById('name1');
    if (!name){
        parText.innerHTML = "Please, submit required data";
        return false;
    }else{
        parText.innerHTML = "";
        return true;
    }
}
//check all input that is supposed to have numerical values
function priceCheck(){
    var price = document.getElementById('price').value,
    parText = document.getElementById('price1');
    if (!price){
        parText.innerHTML = "Please, submit required data";
        return false;
    }else if(isNaN(price)){
        parText.innerHTML = "Please, provide the data of indicated type";
        return false;
    }else{
        parText.innerHTML = "";
        return true;
    }
}
function sizeCheck(){
    var input = document.getElementById('size').value,
    parText = document.getElementById('size1');
    if (!input){
        parText.innerHTML = "Please, submit required data";
        return false;
    }else if(isNaN(input)){
        parText.innerHTML = "Please, provide the data of indicated type";
        return false;
    }else{
        parText.innerHTML = "";
        return true;
    }
}
function weightCheck(){
    var input = document.getElementById('weight').value,
    parText = document.getElementById('weight1');
    if (!input){
        parText.innerHTML = "Please, submit required data";
        return false;
    }else if(isNaN(input)){
        parText.innerHTML = "Please, provide the data of indicated type";
        return false;
    }else{
        parText.innerHTML = "";
        return true;
    }
}
function widthCheck(){
    var input = document.getElementById('width').value,
    parText = document.getElementById('width1');
    if (!input){
        parText.innerHTML = "Please, submit required data";
        return false;
    }else if(isNaN(input)){
        parText.innerHTML = "Please, provide the data of indicated type";
        return false;
    }else{
        parText.innerHTML = "";
        return true;
    }
}
function lengthCheck(){
    var input = document.getElementById('length').value,
    parText = document.getElementById('length1');
    if (!input){
        parText.innerHTML = "Please, submit required data";
        return false;
    }else if(isNaN(input)){
        parText.innerHTML = "Please, provide the data of indicated type";
        return false;
    }else{
        parText.innerHTML = "";
        return true;
    }
}
function heightCheck(){
    var input = document.getElementById('height').value,
    parText = document.getElementById('height1');
    if (!input){
        parText.innerHTML = "Please, submit required data";
        return false;
    }else if(isNaN(input)){
        parText.innerHTML = "Please, provide the data of indicated type";
        return false;
    }else{
        parText.innerHTML = "";
        return true;
    }
}

